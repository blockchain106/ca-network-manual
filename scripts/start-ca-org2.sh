
export PATH=${PWD}/bin/:$PATH
export FABRIC_CA_SERVER_OPERATIONS_LISTENADDRESS=0.0.0.0:18054

fabric-ca-server start -b admin:adminpw -d -n ca-org2 --home "${PWD}/ca/ca-org2" --port 8054 --tls.enabled