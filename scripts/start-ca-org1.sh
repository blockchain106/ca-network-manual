
export PATH=${PWD}/bin/:$PATH
export FABRIC_CA_SERVER_OPERATIONS_LISTENADDRESS=0.0.0.0:17054

fabric-ca-server start -b admin:adminpw -d -n ca-org1 --home "${PWD}/ca/ca-org1" --port 7054 --tls.enabled