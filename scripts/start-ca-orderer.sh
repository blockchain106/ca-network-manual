
export PATH=${PWD}/bin/:$PATH
export FABRIC_CA_SERVER_OPERATIONS_LISTENADDRESS=0.0.0.0:19054

fabric-ca-server start -b admin:adminpw -d -n ca-orderer --home "${PWD}/ca/ca-orderer" --port 9054 --tls.enabled